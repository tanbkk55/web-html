var paramshomesec1_1 = {
    container: document.getElementById('home-sec-1-1'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: homesec1_1
};

var animhomesec1_1;
animhomesec1_1 = lottie.loadAnimation(paramshomesec1_1);

var paramshomesec1_4 = {
    container: document.getElementById('home-sec-1-4'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: homesec1_1
};

var animhomesec1_4;
animhomesec1_4 = lottie.loadAnimation(paramshomesec1_4);

var paramshomesec1_2 = {
    container: document.getElementById('home-sec-1-2'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: homesec1_2
};

var animhomesec1_2;
animhomesec1_2 = lottie.loadAnimation(paramshomesec1_2);



var paramssec2_3whiz = {
    container: document.getElementById('home-sec-2-3whiz'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: sec2_3whiz
};

var animhomesec2_3whiz;
animhomesec2_3whiz = lottie.loadAnimation(paramssec2_3whiz);

var paramssec3_3whiz = {
    container: document.getElementById('home-sec-3-3whiz'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: sec2_3whiz
};

var animhomesec3_3whiz;
animhomesec3_3whiz = lottie.loadAnimation(paramssec3_3whiz);

// var animationHome2 = 

// var params2 = {
//     container: document.getElementById('home-2'),
//     renderer: 'svg',
//     loop: true,
//     autoplay: true,
//     animationData: animationHome2
// };

// var anim2;
// anim2 = lottie.loadAnimation(params2);

// //////////////////////////////////////////


// var animationHome3 = 
// var params3 = {
//     container: document.getElementById('home-3'),
//     renderer: 'svg',
//     loop: true,
//     autoplay: true,
//     animationData: animationHome3
// };

// var anim3;
// anim3 = lottie.loadAnimation(params3);

// anim3.addEventListener('DOMLoaded', (e) => {
//     console.log('play', anim3);
// /* 	anim.play(); */
// anim3.playSegments([30, 70], true);
// });

// let anim = lottie.loadAnimation({
//     container: document.getElementById('anim'),
//     renderer: 'svg',
//     loop: true,
//     autoplay: true,
//     path: 'https://labs.nearpod.com/bodymovin/demo/markus/isometric/markus2.json',
//   });
  
//   anim.addEventListener('DOMLoaded', (e) => {
//       console.log('play', anim);
//   /* 	anim.play(); */
//       anim.playSegments([1, 34], true);
//   });

// //////////////////////////////////////////

// Engineering Consultancy System Integration
var engineer = {
    container: document.getElementById('home-engineer'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramsengineer
};
var animengineer;
animengineer = lottie.loadAnimation(engineer);

var engineer_mb = {
    container: document.getElementById('home-engineer-mb'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramsengineer_mb
};
var animengineer_mb;
animengineer_mb = lottie.loadAnimation(engineer_mb);

///////////////////////////////////////////////////

// AI / Machine Learning
var ai = {
    container: document.getElementById('home-ai'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramsai
};
var animai
animai = lottie.loadAnimation(ai);

var ai_mb = {
    container: document.getElementById('home-ai-mb'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramsai_mb
};
var animai_mb
animai_mb = lottie.loadAnimation(ai_mb);

///////////////////////////////////////////////////

// Real-time Big-data Analytics
var analytic = {
    container: document.getElementById('home-analytic'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramsanalytic
};
var animanalytic
animanalytic = lottie.loadAnimation(analytic);

var analytic_mb = {
    container: document.getElementById('home-analytic-mb'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramsanalytic_mb
};
var animanalytic_mb
animanalytic_mb = lottie.loadAnimation(analytic_mb);

///////////////////////////////////////////////////

// Blockchain Technology
var blockchain = {
    container: document.getElementById('home-blockchain'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramsblockchain
};
var animblockchain
animblockchain = lottie.loadAnimation(blockchain);

var blockchain_mb = {
    container: document.getElementById('home-blockchain-mb'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramsblockchain_mb
};
var animblockchain_mb
animblockchain_mb = lottie.loadAnimation(blockchain_mb);

///////////////////////////////////////////////////

// Software / application Developer
var software = {
    container: document.getElementById('home-software'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramssoftware
};
var animsoftware
animsoftware = lottie.loadAnimation(software);

var software_mb = {
    container: document.getElementById('home-software-mb'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramssoftware_mb
};
var animsoftware_mb
animsoftware_mb = lottie.loadAnimation(software_mb);

///////////////////////////////////////////////////

// UX-oriented User-centered Design
var ux = {
    container: document.getElementById('home-ux'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramsux
};
var animux
animux = lottie.loadAnimation(ux);

var ux_mb = {
    container: document.getElementById('home-ux-mb'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: paramsux_mb
};
var animux_mb
animux_mb = lottie.loadAnimation(ux_mb);
